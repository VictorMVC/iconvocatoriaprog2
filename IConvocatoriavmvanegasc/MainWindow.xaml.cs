﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace IConvocatoriavmvanegasc
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            btnVincular.IsEnabled = false; btnGuardar.IsEnabled = true;
            MessageBox.Show("Por favor crear una carpeta llamada (Examen) en el disco C para guardar las listas");
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Guardado Correctamente");
            btnVincular.IsEnabled = true;
            txtCorreo.Text = "";
            txtNombre.Text = "";
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Vinculando");
            Pedido ped = new Pedido();
            ped.ShowDialog();

            MainWindow mw = new MainWindow ();
            mw.Close();


        }
    }
}
